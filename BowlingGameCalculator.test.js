import BowlingGameCalculator from "./BowlingGameCalculator";

describe("BowlingGame Calculator", () => {
  let calculator;

  beforeEach(() => {
    calculator = new BowlingGameCalculator();
  });

  it("returns the score", () => {
    expect(calculator.score()).toEqual(9);
  });
});
