# Bowling Game Calculator

### Welcome to the Bowling League!

We are excited to have you join us, but we need your help! Our league currently has no way of scoring our bowling games and are looking to you to build it for us!

Don't worry if you don't complete this! We are looking for someone that approaches this problem, explains their methods and reasons and uses us as your resource.

We are test minded people here and would love for you to test drive your implementation.

**Need help? Use us as your engineering pair or use Stack Overflow, we all do it!**

## Files

- BowlingGameCalculator.js => implementation file
- BowlingGameCalculator.test.js => testing file

_Feel free to also break out things into your own files as needed._

## Requirements

1. Start off by building one frame. A frame consists of two rolls that total no more than 10 pins kncoked down.
2. Now build a game that allows for n+1 frames. Remember, a complete game has no more than 10 frames
3. Can you out put your code to some UI? It would be great to see the bowling game in form using html and css
4. **Extra Credit:** _Only complete if all of the above is done_. We all know that bowling comes with some nice bonus points. Implement strikes and spare scoring for the frames.

#### Scoring for strikes and spares

When a strike is bowled, the bowler is awarded the score of 10 (for knocking down all ten pins), plus he gets to add the total of his next two rolls to that frame. For a spare, the bowler gets the 10, plus the total number of pins knocked down on the next roll only.
